module System.Path.Part (
    -- * Possible types for Path type parameters
    Abs,
    Rel,
    AbsRel,
    File,
    Dir,
    FileDir,
    ) where

import System.Path.Internal.Part
