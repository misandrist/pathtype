module System.Path.PartClass (
    AbsRel(..), AbsOrRel(..),
    FileDir(..), FileOrDir(..),
    ) where

import System.Path.Internal.PartClass
